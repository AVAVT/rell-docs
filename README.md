# Readme

Rell docs go here.

## Readthedocs

We use readthedocs to build our docs into a static HTML site. The site is hosted on readthedocs servers, and reachable at rell.readthedocs.io or at rell.chromia.com.

When changes are pushed to master, the documentation will automatically build and will be available after a few minutes.

Rell docs are in reStructuredText format (http://docutils.sourceforge.net/rst.html).

## Admin

Gus is currently responsible for maintaining this repo, and administers readthedocs. Speak to him if you need something.

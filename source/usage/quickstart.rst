Quick start
===========

This document should help you get up and running with the Rell IDE.

Install Docker
~~~~~~~~~~~~~~

A software called Docker is required in order to run the IDE.
It can be downloaded from docker.io_, you will also have to create an account.

Install the IDE
~~~~~~~~~~~~~~~

This tutorial assumes that you are using Linux or a Mac OS, and that you have a certain familiarity with *nix operating systems and tools. It is possible to install the IDE on Windows, but we do not have specific instructions for doing so. We hope to add them in future.

1. Create a new directory called ``relltest``
2. Create a file called ``docker-compose.yml`` and paste *all* of the following into it:

::

    # Copyright (c) 2017 ChromaWay Inc. See README for license information.

    version: '3.1'

    services:

      postgres:
        image: chromaway/postgres:2.4.3-beta
        restart: always
        environment:
          POSTGRES_PASSWORD: postchain

      dev-preview:
        image: chromaway/dev-preview:0.8.0-b4
        restart: always
        ports:
          - 127.0.0.1:7740:7740
          - 127.0.0.1:30000:30000
        depends_on:
          - postgres

3. Open a terminal in the ``relltest`` directory and type ``docker-compose up``. When the terminal stops writing, you should be able to try the IDE, see next section.

.. note::
  You may need to use ``sudo`` to execute ``docker-compose``.

Try the IDE
~~~~~~~~~~~

Point your browser to http://localhost:30000. You should see the Rell IDE.

Click Select Module (first element in the top bar) and then Create. A new window will appear. Write any name for your module, such as Hello.

In the dropdown, select "Rell", and select the "Use template for code and tests" option.

|Create Module Button|

This will open a modal element where you can specify the name of the Module and the Language used (in this example: Rell). For convenience you can include template and test code.

|Create Options|

Click the blue "Create" button. The screen will now be filled with code.

|Template|

Browser
    To the left bar is the Browser. You can use it to work with several examples.

Editor
    Inside the central element on the left the editor filled with a template of source code.

Test window
    Inside the central element on the right there is a window with XML. This is a way to run your code, simulating a real application.

Buttons
    On top of the Editor there  is a button "Start Node", don't press that one yet.
    There is also a button "Run tests". Click it to see that the test passes.

Hello World!
~~~~~~~~~~~~

As a minimal first application, you can make a Hello World example with a focus on Ukraine.

If you checked the *use template* box and look at editor section on the top, you will see this code as a template:


::

    class city { key name; }

    operation insert_city (name) {
        create city (name);
    }


This is a small registry of cities. In order to run the code we need a test in XML.

::

    <test>
    <block>
        <transaction>
        <signers><param type="bytea" key="Alice"/></signers>
        <operations>
            <operation name="insert_city">
            <string>Kiev</string>
            </operation>
        </operations>
        </transaction>
    </block>
    </test>

Click the 'Run tests' button, and a green message will appear.

|Success|


After all this work, we suggest that you put "Relational Blockchain" on your CV.

Next step is to learn more about Rell in the :doc:`Tutorial </tutorial/reltut>`.

However if you feel eager to click the *Start Node* button and
create a complete running application, you can
learn how to use the javascript client library in the :doc:`Client </client/client-intro>` section.

Videos
~~~~~~

If you are stuck, we have some videos showing how to get started, on a Mac.

With_docker_installed_installing_developer_preview_

.. _docker.io: https://www.docker.com/products/docker-desktop
.. _With_docker_installed_installing_developer_preview: https://youtu.be/naiODFFPLgE

.. |Create Module Button| image:: create-module-button.png
  :width: 600
  :alt: Where to create a new module

.. |Create Options| image:: create-rell-module.png
  :width: 600
  :alt: Create Module options window


.. |Template| image:: postchain-ide-with-template.png
  :width: 600
  :alt: The IDE with a template

.. |Success| image:: kiev-success.png
  :width: 600
  :alt: The IDE shows Run Succesfully

Introduction
============

Rell SDK
---------

Welcome to the first Rell SDK! This is a milestone in the development of Chromia, and one of the first opportunities to get your hands on the latest tools that have been developed for the platform. In line with our mission to make mainstream dapps a practical reality, we have decided to prioritise providing a rich and usable toolset for dapp developers right from the outset.

This early SDK consists of our Rell IDE which allows you to write and compile Rell code, as well as language documentation, code samples, and a simple tutorial. We will be expanding this material as Rell matures and develops. Right now it gives the dapp developer community an early look at what they will be able to achieve with Chromia and Rell. 

We welcome any and all feedback, please send bug reports and suggestions to feedback@chromia.com

Chromia
-------

Rell is built for Chromia. Chromia is a new blockchain platform for decentralized applications, conceived in response to the shortcomings of existing platforms and designed to enable a new generation of dapps to scale beyond what is currently possible

While platforms such as Ethereum allow any kind of application to be implemented in theory, in practice they have many limitations: bad user experience, high fees, frustrating developer experience, poor security. This prevents decentralized apps (dapps) from going mainstream.

We believe that to address these problems properly we need to seriously rethink the blockchain architecture and programming model with the needs of decentralized applications in mind. Our priorities are to: 

* Allow dapps to scale to millions of users.
* Improve the user experience of dapps to achieve parity with centralized applications.
* Allow developers to build secure applications with using familiar paradigms.

Rell language
-------------

Most dapp blockchain platforms use virtual machines of various kinds. But a traditional virtual machine architecture doesn’t work very well with the Chromia relational data model, as we need a way to encode queries as well as operations. For this reason, we are taking a more language-centric approach: a new language called Rell (Relational language) will be used for dapp programming.  This language allows programmers to describe the data model/schema, queries, and procedural application code.

Rell code is compiled to an intermediate binary format which can be understood as code for a specialized virtual machine. Chromia nodes will then translate queries contained in this code into SQL (while making sure this translation is safe) and execute code as needed using an interpreter or compiler.

Rell has the following features:

* Type safety / static type checks. It’s very important to catch programming errors at the compilation stage to prevent financial losses. Rell is much more type-safe than SQL, and it makes sure that types returned by queries match types used in procedural code.
* Safety-optimized. Arithmetic operations are safe right out of the box, programmers do not need to worry about overflows. Authorization checks are explicitly required.
* Concise, expressive and convenient. Many developers dislike SQL because it is highly verbose. Rell doesn’t bother developers with details which can be derived automatically. As a data definition language, Rell is up to 7x more compact than SQL. 
* Allows meta-programming. We do not want application developers to implement the basics from scratch for every dapp. Rell will allow functionality to be bundled as templates.

Our research indicated that no existing language or environment has this feature set, and thus development of a new language was absolutely necessary.

We designed Rell in such a way that it is easy to learn for programmers:

* Programmers can use relational programming idioms they are already familiar with. However, they don’t have to go out of their way to express everything through relational algebra: Rell can seamlessly merge relational constructs with procedural programming.
* The language is deliberately similar to modern programming languages like JavaScript and Kotlin. A familiar language is easier to adapt to, and our internal tests show that programmers can become proficient in Rell in matter of days. In contrast, the ALGOL-style syntax of PL/SQL generally feels unintuitive to modern developers.


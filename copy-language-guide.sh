#!/bin/bash
set -eu
D=$(dirname $0)
rm $D/source/languageguide/*.rst
cp $D/../rellr/doc/guide/* $D/source/languageguide
echo "Language Guide copied"
